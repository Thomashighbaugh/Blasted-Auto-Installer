#!/bin/bash

yay -aSyu --noconfirm rcm

git clone https://github.com/Thomashighbaugh/dotfiles

cd dotfiles

rcup *

env RCRC=/dev/null rcup -B 0 -g > install.sh
