#!/bin/bash

# Installs BlackArch Repo and (optionally) its tools. 

# Run https://blackarch.org/strap.sh as root and follow the instructions.
 curl -O https://blackarch.org/strap.sh

# The SHA1 sum should match: 9f770789df3b7803105e5fbc19212889674cd503 strap.sh
 sha1sum strap.sh

# Set execute bit
 chmod +x strap.sh

# Run strap.sh
 sudo ./strap.sh 

  sudo pacman -S blackarch 
