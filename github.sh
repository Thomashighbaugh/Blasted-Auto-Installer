sudo pacman -Syu
cd /tmp/

git clone https://github.com/mig/gedit-themes
cd gedit-themes
sudo cp -rnv * /usr/share/gtksourceview-4/styles && cd /tmp/

# TMUX Package Manager
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
echo "# List of plugins" >> ~/.tmux.conf
echo "set -g @plugin 'tmux-plugins/tpm'" >> ~/.tmux.conf
echo "set -g @plugin 'tmux-plugins/tmux-sensible'" >> ~/.tmux.conf
echo "# Other examples:" >> ~/.tmux.conf
echo "# set -g @plugin 'github_username/plugin_name'" >> ~/.tmux.conf
echo "# set -g @plugin 'git@github.com/user/plugin'" >> ~/.tmux.conf
echo "# set -g @plugin 'git@bitbucket.com/user/plugin'" >> ~/.tmux.conf
echo "# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)" >> ~/.tmux.conf
echo "run -b '~/.tmux/plugins/tpm/tpm'" >> ~/.tmux.conf
tmux source ~/.tmux.conf


cd /opt
wget https://aur.archlinux.org/cgit/aur.git/snapshot/xst-git.tar.gz
tar xvf xst-git.tar.gz
cd xst-git
makepkg -si

cd /opt
wget https://aur.archlinux.org/cgit/aur.git/snapshot/nerd-fonts-iosevka.tar.gz
tar xvf nerd-fonts-iosevka.tar.gz
cd nerd-fonts-iosevka
makepkg -si
