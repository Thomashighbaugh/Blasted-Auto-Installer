#!/bin/bash 

echo "Development Tools"
sudo snap install e-tools
sudo snap install coulr
sudo snap install odrive-unofficial

echo "Photo Manipulation"
sudo snap install boxy-svg
sudo snap install xnsketch
sudo snap install lanto
sudo snap install krop
sudo snap install amparepngtoico
sudo snap install mountain-tapir
sudo snap install photoscape
sudo snap install imagenes
sudo snap install pencilsheep
sudo snap install bobrossquotes
sudo snap install krita
sudo snap install figma-linux
 sudo snap install imeditor


echo "Web Browsers"
sudo snap install brave --classic

echo "BitCoin Wallets" 
snap install electrum

## Crypto Miners
# sudo snap install xmr-miner-gui

echo "P2P Net Browsers"
sudo snap install zeronet


