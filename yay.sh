#!/bin/sh
yay -Syu
# yay -S --noconfirm  debtap
# yay -S --noconfirm  deb2appimage
yay -S --noconfirm  appimagelauncher
yay -S --noconfirm  oomox
# yay -S --noconfirm  gtk-engine-aurora
yay -S --noconfirm  appimagelauncher
# yay -S --noconfirm  gtk-engine-unico
# yay -S --noconfirm  notable-bin
# yay -S --noconfirm  mindforger-src
# yay -S --noconfirm  mdp
# yay -S --noconfirm  vnote
# yay -S --noconfirm  vim-markdown
yay -S --noconfirm  menulibre
yay -S --noconfirm  android-messages-desktop
yay -S --noconfirm  coulr
# yay -S --noconfirm  kotlin-native
# yay -S --noconfirm  bitwarden
yay -S --noconfirm  arch-audit 
# yay -S --noconfirm  nvidia-docker
# yay -S --noconfirm  dockstation
# yay -S --noconfirm  lxd
yay -S --noconfirm  distrobuilder
yay -S --noconfirm  appeditor
yay -S --noconfirm  firetools
yay -S --noconfirm  wpgtk-git 
yay -S --noconfirm  pngcrush-bundled
yay -S --noconfirm  vboxtool
#yay -S --noconfirm  numix-icon-theme-git
yay -S --noconfirm  octopi
yay -S --noconfirm  pkgbrowser 
yay -S --noconfirm  argon
yay -S --noconfirm  cylon
yay -S --noconfirm  nerd-fonts-source-code-pro
# yay -S --noconfirm  vertex-maia-themes
# yay -S --noconfirm  ant-nebula-gtk-theme
# yay -S --noconfirm  ant-dracula-gtk-theme
# yay -S --noconfirm  ant-bloody-gtk-theme
# yay -S --noconfirm spotify
yay -S --noconfirm xsettingsd-git
yay -S --noconfirm polybar-git
yay -S --noconfirm tuned-git
# yay -S --noconfirm lemonbar-git 
# yay -S --noconfirm awesome-git
# yay -S --noconfirm i3-gaps 
