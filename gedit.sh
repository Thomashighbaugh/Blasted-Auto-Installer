
#!/usr/bin/env bash
# configure gedit

gsettings set org.gnome.gedit.preferences.editor tabs-size "4"
gsettings set org.gnome.gedit.preferences.editor insert-spaces "true"
gsettings set org.gnome.gedit.preferences.editor highlight-current-line "true"
gsettings set org.gnome.gedit.preferences.editor display-line-numbers "true"
gsettings set org.gnome.gedit.preferences.editor bracket-matching "true"
gsettings set org.gnome.gedit.preferences.editor syntax-highlighting "true"
gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline "true"
gsettings set org.gnome.gedit.preferences.editor auto-save true
gsettings set org.gnome.gedit.preferences.editor auto-save-interval 1
