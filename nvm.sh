 #!/bin/zsh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm" # Add to ~.zshrc/~.bashrc
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # Load NVM
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # Completions

/bin/sh
echo " "
echo " "
echo " "
echo " "
echo " "
echo "Installing Global Packages from Node"
nvm install node
npm install -g yarn
yarn add global pnpm
yarn add global prettier



