#!/bin/bash

echo "Blasted Auto Installer"
echo "Manjaro Edition"
echo 
echo
echo

read -s -r -p "Press any key to continue"
clear



echo "Install Packages Using Pacman"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
sh $PWD/pacman.sh
fi
wait 5
clear

echo "Install AUR Packages Using Yay"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
sh $PWD/aura.sh
fi


wait 5  
echo "Install From GitHub"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
sh $PWD/github.sh
fi
wait 5
clear


  wait 5  
echo "Install SpaceMacs"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
sh $PWD/spacemacs.sh 
(emacs)
fi
wait 5
clear



echo "Install Dotfiles"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
sh dots.sh
fi
wait 5
clear

echo "Install BlackArch"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
sh $PWD/black.sh
fi
wait 5
clear

echo "Reboot"
read -p "Continue? " -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then

wait 10
sudo reboot
fi
  
